(ns clj-db-lib.utils.strict
  (:require [clj-data-lib.data :as cld]
            [clj-data-lib.string :refer [ci= ci-in-coll?]]
            [clj-data-lib.time :as time]
            [clj-data-lib.translate :as trn]
            [clj-db-lib.spec.sql :as o]
            [clj-db-lib.provider :as p]
            [clj-db-lib.utils.data :refer [valid?]]
            [clj-db-lib.utils.exceptions :refer [validation-error]]
            [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [expound.alpha :refer [expound-str]]
            [taoensso.timbre :as log]))

(def strict-type-fns
  {:bool          trn/type->bool
   :bigint        biginteger
   :date          time/type->local-date
   :datetime      time/type->local-date-time
   :int           trn/type->int
   :numeric       bigdec
   :numeric-float trn/type->float})

(defn map->strict-opts [strict-opts]
  (let [qm (cld/map->qualified ::o/strict-opts strict-opts)]
    (when-not (s/valid? ::o/strict-opts qm)
      (throw (validation-error "Invalid strict-opts spec" (expound-str ::o/strict-opts qm))))
    qm))

(defn table->str [table]
  (when table
    (let [[tbl schema] (reverse (string/split (name table) #"\."))]
      [schema tbl])))

(defn- dt->dtk [dt provider]
  (->>  (p/strict-map provider)
        :data-type
        (some
         #(let [[dtk data-types] %]
            (when (ci-in-coll? dt data-types) dtk)))))

(defn- get-column-dtk [table-ddl column provider]
  (when-let [dt (some #(when (ci= (:column_name %) (name column)) (:type_name %)) table-ddl)]
    (dt->dtk dt provider)))

(defn get-strict-map
  "Generate a strict-map to be used by data->strict. Map generated as {vector-pos dtk}"
  [table-ddl {:keys [::o/columns ::o/provider]}]
  (log/debug "Strict type column structure:" (string/join ", " columns))
  (->> columns
       (map-indexed (fn [idx itm]
                      (when-let [dtk (get-column-dtk table-ddl itm provider)]
                        (when (get strict-type-fns dtk)
                          {idx dtk}))))
       (remove nil?)
       (into {})))

(defmulti data-set->strict (fn [data-set _ _] (map? data-set)))
(defmethod data-set->strict true [data-row table-ddl strict-opts]
  (let [columns (vec (keys data-row))]
    (-> (cld/data-maps->vector columns [data-row])
        (data-set->strict table-ddl (assoc strict-opts ::o/columns columns))
        (#(cld/vector->data-maps columns %))
        first)))
(defmethod data-set->strict false [data-set table-ddl {:keys [::o/columns] :as strict-opts}]
  (let [strict-map (get-strict-map table-ddl strict-opts)]
    (log/debug "Strict type provider, applying map: " strict-map "\nusing opts: " strict-opts)
    (valid? (first data-set) columns)
    (map
     (fn [row]
       (reduce
        (fn [d1 [k1 v1]]
          (update-in d1 [k1] (get strict-type-fns v1)))
        (vec row)
        strict-map))
     data-set)))
