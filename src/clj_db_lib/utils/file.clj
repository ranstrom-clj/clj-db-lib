(ns clj-db-lib.utils.file
  (:require [clj-data-lib.data :as cld]
            [clj-data-lib.time :as time]
            [clj-data-lib.translate :refer [str->char]]
            [clj-refile.core :refer [map->json-file]]
            [clj-refile.parse :as fp]
            [clj-refile.formats.csv :as c]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [taoensso.timbre :as log]))

(defn create-and-apply-manifest [file-directory files]
  (let [files-count (count files)]
    (if (> files-count 1)
      {:files-extracted    files
       :file-manifest-path (map->json-file
                            (string/join "/" [file-directory (string/join [(cld/uuid) ".manifest"])])
                            {:entries (->> files (map #(hash-map :url % :mandatory true)) vec)})}
      {:files-extracted files})))

(defn data->file
  [output-path {:keys [gzip include-header separator columns reducer-fn-out]} data]
  (let [gzip? (cld/or+ gzip true)
        include-header? (cld/or+ include-header true)
        output-filepath (if gzip? (str output-path ".gz") output-path)
        separator-char (str->char separator)
        output-columns (->> (or reducer-fn-out columns (-> data first keys))
                            (map name) vec)
        file-output-map {:output-filepath output-filepath :gzip gzip? :include-header include-header?
                         :separator-char  separator-char :header output-columns}]
    (log/info "Extracting data to: " output-path)
    (io/make-parents output-path)
    (->> (c/write-to-file-multiple file-output-map data)
         (create-and-apply-manifest (fp/get-dir-from-path output-path))
         (merge {:last_time_updated (time/instant-as-str) :output_columns output-columns}))))