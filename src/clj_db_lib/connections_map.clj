(ns clj-db-lib.connections-map
  (:require [clj-data-lib.data :refer [->unqualified]]
            [clj-db-lib.connections :as c]
            [taoensso.timbre :as log])
  (:import (clj_db_lib.connections DBPooledSpec)))

(defprotocol ConnectionMap
  (setup [this properties conn-vector])
  (close [this]))

(defrecord DBConnectionMap []
  ConnectionMap
  (setup [this properties conn-vector]
    (reduce
     (fn [conn-map c]
       (assoc conn-map (->unqualified c) (c/db-pooled-spec (properties c))))
     this conn-vector))
  (close [this]
    (doseq [[k ^DBPooledSpec v] this]
      (log/debug "closing connection for: " k)
      (.close v))))

(defn db-conn-map [properties conn-vector]
  (.setup (DBConnectionMap.) properties conn-vector))