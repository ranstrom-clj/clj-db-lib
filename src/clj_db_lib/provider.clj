(ns clj-db-lib.provider
  (:require [cheshire.core :as cc]
            [clj-data-lib.data :refer [qualified-map->unqualified]]
            [clj-db-lib.spec.connections :as c]
            [clj-http.client :as http-client]
            [clojure.string :as string]))

; Provider connection info

(defn- construct-jdbc-url [subprotocol subname]
  (str "jdbc:" subprotocol ":" subname))

(defmulti db-spec ::c/provider)
(defmethod db-spec :h2
  [{:keys [::c/h2_path ::c/dbname]}]
  {:adapter "h2"
   :url     (construct-jdbc-url
             (if h2_path "h2:file" "h2:mem")
             (or h2_path (str (or dbname "demo") ";DB_CLOSE_DELAY=-1")))})
(defmethod db-spec :mssql
  [{:keys [::c/host ::c/port ::c/dbname ::c/user ::c/pswd ::c/auth_scheme ::c/read_only
           ::c/ds-props]}]
  (let [dpm (qualified-map->unqualified ds-props)]
    (if (and user pswd)
      {:adapter       "sqlserver"
       :server-name   host
       :database-name dbname}
      {:jdbc-url (construct-jdbc-url
                  "sqlserver"
                  (str "//" host ":" (or port 1433) ";database=" dbname
                       (when (nil? (:authentication dpm)) ";integratedSecurity=true")
                       (when read_only ";applicationIntent=readonly")
                       ; TODO Sunset in favor of ds-props
                       (when (and auth_scheme (nil? (:authenticationScheme dpm)))
                         (str ";authenticationScheme=" auth_scheme))))})))
(defmethod db-spec :openedge
  [{:keys [::c/host ::c/port ::c/dbname]}]
  {:jdbc-url (construct-jdbc-url
              "datadirect:openedge"
              (str "//" host ":" port ";databaseName=" dbname))})
(defmethod db-spec :oracle
  [{:keys [::c/host ::c/port ::c/dbname ::c/sid]}]
  {:jdbc-url (construct-jdbc-url
              "oracle:thin"
              (if dbname
                (str "@//" host ":" (or port 1521) "/" dbname)
                (str "@" host ":" (or port 1521) ":" sid)))})
(defmethod db-spec :postgres
  [{:keys [::c/host ::c/port ::c/dbname ::c/read_only ::c/pool-name]}]
  {:adapter       "postgresql"
   :server-name   host
   :database-name dbname
   :port-number   (or port 5432)
   :pool-name     (or pool-name "clj-db-pool")
   ; auto-commit to improve performance when streaming large results
   :auto-commit   (if read_only false true)})
(defmethod db-spec :snowflake
  [{:keys [::c/host ::c/port ::c/dbname ::c/warehouse ::c/role]}]
  {:jdbc-url (construct-jdbc-url
              "snowflake"
              (str "//" host ":" (or port 443) "/?"
                   (when dbname (str "&db=" dbname))
                   (when warehouse (str "&warehouse=" warehouse))
                   (when role (str "&role=" role))))})
(defmethod db-spec :synapse [v]
  (db-spec (assoc v ::c/provider :mssql)))
(defmethod db-spec :databricks
  [{:keys [::c/host ::c/httpPath ::c/schema]}]
  {:jdbc-url (format "jdbc:databricks://%s:443/%s;HttpPath=%s;UseNativeQuery=1"
                     host schema httpPath)})
(defmethod db-spec :default [{:keys [::c/provider]}]
  (throw (Exception. (str "Unsupported provider: " provider))))

(defmulti system-props ::c/provider)
(defmethod system-props :oracle [{:keys [::c/application-name]}]
  (let [os-user (System/getProperty "user.name")]
    (merge {"oracle.jdbc.J2EE13Compliant" "true"}
           (when (> (count os-user) 30)
             {"oracle.jdbc.v$session.program" (or application-name "Unidentified")
              "oracle.jdbc.v$session.osuser"  (subs os-user 0 (min (count os-user) 30))}))))
(defmethod system-props :default [_] {})

(defn- ->env [v]
  (let [sv (System/getenv v)]
    (when-not (string/blank? sv) sv)))

(defmulti db-token ::c/provider)
(defmethod db-token :postgres [{:keys [::c/token_provider ::c/client_id]}]
  (case (keyword token_provider)
    :azure
    (let [resource "https%3A%2F%2Fossrdbms-aad.database.windows.net"
          uri-str (str (->env "IDENTITY_ENDPOINT") "/?resource=" resource "&api-version=2019-08-01"
                       (when client_id (str "&client_id=" client_id)))
          ret (http-client/get uri-str {:headers {"X-IDENTITY-HEADER" (->env "IDENTITY_HEADER")}})]
      (-> ret :body (cc/parse-string keyword) :access_token))
    nil))
(defmethod db-token :default [_] nil)

(defmulti add-ds-props ::c/provider)
(defmethod add-ds-props :oracle [_] {:defaultRowPrefetch 4000})
;(defmethod add-ds-props :postgres [_] {:reWriteBatchedInserts true})
(defmethod add-ds-props :synapse [_]
  {:encrypt true
   :trustServerCertificate "false"
   :hostNameInCertificate "*.database.windows.net"})
(defmethod add-ds-props :databricks [{:keys [::c/pswd]}]
  {"PWD" pswd})
(defmethod add-ds-props :default [_] {})

(defmulti session-queries ::c/provider)
(defmethod session-queries :oracle [_]
  (vector
   (string/join
    "\n"
    ["BEGIN"
     "EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''YYYY-MM-DD HH24:MI:SS''';"
     "EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_TIMESTAMP_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF3''';"
     "EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_TIMESTAMP_TZ_FORMAT = ''YYYY-MM-DD HH24:MI:SS.FF3 TZR''';"
     "EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_LENGTH_SEMANTICS = ''BYTE''';"
     "END;"])))
(defmethod session-queries :snowflake [_]
  ["ALTER SESSION SET MULTI_STATEMENT_COUNT=0"])
(defmethod session-queries :synapse [_]
  ["SET DATEFORMAT ymd"])
(defmethod session-queries :default [_] [])

; HoneySQL options

(defmulti hh-quoting keyword)
(defmethod hh-quoting :h2 [_] :oracle)
(defmethod hh-quoting :mssql [_] :sqlserver)
(defmethod hh-quoting :oracle [_] :oracle)
(defmethod hh-quoting :postgres [_] :ansi)
(defmethod hh-quoting :snowflake [_] :oracle)
(defmethod hh-quoting :synapse [_] :sqlserver)
(defmethod hh-quoting :databricks [_] :mysql)
(defmethod hh-quoting :default [_] :ansi)

(defn hh-opts [{:keys [provider quote-when-fn]}]
  {:quoting (hh-quoting provider)
   :quote-when-fn quote-when-fn})

; Provider SQL

(def batch-providers [:mssql :oracle :synapse])
(def single-api-query-providers [:mssql :synapse :databricks])
(def no-transaction-providers [:synapse])

(defn- provider-in-group? [provider group]
  (if (some #{(keyword provider)} group) true false))

(defn batch-provider? [provider] (provider-in-group? provider batch-providers))
(defn single-api-query-provider? [provider] (provider-in-group? provider single-api-query-providers))
(defn no-transaction-provider? [provider] (provider-in-group? provider no-transaction-providers))

(defmulti strict-map keyword)
(defmethod strict-map :postgres [_]
  {:data-type {:bool          ["boolean" "bool"]
               :bigint        ["bigint" "int8" "bigserial" "serial8"]
               :date          ["date"]
               :datetime      ["timestamp" "timestamp without timezone" "time"
                               "time without timezone" "timestamp without time zone"]
               :int           ["integer" "smallint" "smallserial" "serial" "int4"]
               :numeric       ["numeric" "money"]
               :numeric-float ["double precision" "real" "float4" "float8"]}})
(defmethod strict-map :default [_] {})

(defmulti provider-batch-size keyword)
(defmethod provider-batch-size :synapse [_] 100)
(defmethod provider-batch-size :default [_] nil)

(defn strict-type-provider? [provider]
  (->> (keys (methods strict-map))
       (map keyword)
       (filter #(not= :default %))
       set
       (provider-in-group? provider)))

(defn default-max-retries-for-provider
  "Return the default number of times a query should be retired after
  an exception."
  [provider]
  (let [max-retries {:databricks 3}]
    (get max-retries
         (keyword (string/lower-case (or provider "")))
         0)))

(defmulti provider-read-query-opts keyword)
(defmethod provider-read-query-opts :postgres [_]
  {:fetch-size 4000, :concurrency :read-only, :cursors :close, :result-type :forward-only})
(defmethod provider-read-query-opts :default [_] {})

(defmulti provider-insert-opts keyword)
(defmethod provider-insert-opts :synapse [_]
  {:return-keys false})
(defmethod provider-insert-opts :default [_] {})
