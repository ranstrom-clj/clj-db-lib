(ns clj-db-lib.spec.sql
  (:require [clj-db-lib.spec.connections :refer [supported-providers]]
            [clojure.spec.alpha :as s]))

(defn function? [v] (= (type v) clojure.lang.IFn))

(s/def ::provider supported-providers)
(s/def ::table (s/or :k keyword? :s string?))
(s/def ::columns (s/* (s/or :k string? :s keyword?)))

(s/def ::strict-opts
  (s/keys :req [::provider ::table]
          :opt [::columns]))

(s/def ::gzip boolean?)
(s/def ::include-header boolean?)
(s/def ::limit-rows integer?)
(s/def ::separator (s/or :s (s/and string? #(= (count %) 1))
                         :c char?))
(s/def ::reducer-fn function?)
(s/def ::quote-when-fn function?)
(s/def ::reducer-fn-out (s/* (s/or :k string? :s keyword?)))

(s/def ::execute-opts
  (s/keys :req [::provider]
          :opt [::gzip ::include-header ::limit-rows ::separator
                ::reducer-fn ::quote-when-fn ::reducer-fn-out
                ::columns ::table]))