(ns clj-db-lib.utils.data-test
  (:require [clj-db-lib.utils.data :as dd]
            [clojure.test :refer [deftest is testing]]))

(deftest test-string-in-vector-cleanup
  (testing "cleaning up various vectors with strings"
    (let [f (dd/clean-row-fn nil)]
      (is (= (f [123 "   \n\tABC\n\t\n    "])
             [123 "ABC"])))))

(deftest test-data->reducer-fn
  (testing "data->reducer-fn"
    (let [f1 (dd/data->reducer-fn
              {:separator "|"
               :columns [:a :b]})]
      (is (= (reduce f1 [] [[1 "  some|field  "]])
             [[1 "some field"]])))
    (let [f2 (dd/data->reducer-fn
              {:separator "|"
               :reducer-fn #(update-in % [:a] inc)
               :columns [:B :A]})]
      (is (= (reduce f2 [] [{:a 1 :b " some|field "}])
             [["some field" 2]])))
    (let [f3 (dd/data->reducer-fn
              {:separator "|"
               :reducer-fn
               (fn [row]
                 (reduce
                  (fn [out k1]
                    (conj out {:key k1 :value (get row k1)}))
                  []
                  [:a :b]))
               :reducer-fn-returns-ds true
               :columns [:key :value]})]
      (is (= (reduce f3 [] [{:a 1 :b " some|field "}])
             [[:a 1] [:b "some field"]])))))